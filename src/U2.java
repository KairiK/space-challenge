

public class U2 extends Rocket {


    public U2(){
        cost = 120;
        currentWeight = 18000;
        maxWeight = 29000;

    }


    public boolean launch(){
        double chance = 0.5 * (currentWeight/maxWeight);
        double randomNum = Math.random();
        System.out.println(currentWeight);
        System.out.println(maxWeight);
        System.out.println(chance);
        System.out.println(randomNum);
        if (randomNum <= chance)
            return false;
        else
            return true;
    }

    public boolean land(){
        double chance = 0.08 * (currentWeight/maxWeight);
        double randomNum = Math.random();
        if (randomNum <= chance)
            return false;
        else
            return true;
    }
}

