
public class Rocket implements SpaceShip {
    int cost;
    int currentWeight;
    int maxWeight;

    public Rocket(){
        cost = 0;
        currentWeight = 0;
        maxWeight = 0;
    }
    public int cost(){
        return cost;
    }

    public int currentWeight(){
        return currentWeight;
    }

    public boolean launch(){
        return true;
    }
    public boolean land(){
        return true;
    }
    public boolean canCarry(Item item){
        // a method that takes an Item as an argument and returns true if the rocket can carry such item
        // or false if it will exceed the weight limit.
        if((this.currentWeight + item.weight) < maxWeight){
            return true;
        } else {
            return false;
        }

    }
    public void carry(Item item){
        // a method that also takes an Item object and updates the current weight of the rocket.
        this.currentWeight = this.currentWeight + item.weight;
    }
}
