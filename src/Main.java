import java.util.ArrayList;

public class Main {
    public static void main (String [] args) throws Exception {
        Simulation simulation = new Simulation();
        ArrayList itemsPhase1 = simulation.loadItems("phase-1.txt");
        ArrayList itemsPhase2 = simulation.loadItems("phase-2.txt");
        ArrayList fullU1RocketsP1 = simulation.loadU1(itemsPhase1);
        ArrayList fullU1RocketsP2 = simulation.loadU1(itemsPhase2);
        ArrayList fullU2RocketsP1 = simulation.loadU2(itemsPhase1);
        ArrayList fullU2RocketsP2 = simulation.loadU2(itemsPhase2);
        int budgetU1P1 = simulation.runSimulation(fullU1RocketsP1);
        System.out.println("U1 rockets would cost: " + budgetU1P1 + " million in Phase 1.");

        int budgetU1P2 = simulation.runSimulation(fullU1RocketsP2);
        System.out.println("U1 rockets would cost: " + budgetU1P2 + " million in Phase 2.");

        System.out.println("Total for U1: " + (budgetU1P1 + budgetU1P2) + " million.");

        int budgetU2P1 = simulation.runSimulation(fullU2RocketsP1);
        System.out.println("U2 rockets would cost: " + budgetU2P1 + " million in Phase 1.");

        int budgetU2P2 = simulation.runSimulation(fullU2RocketsP2);
        System.out.println("U2 rockets would cost: " + budgetU2P2 + " million in Phase 2.");

        System.out.println("Total for U2: " + (budgetU2P1 + budgetU2P2) + " million.");
    }
}
