

public class U1 extends Rocket {



    public U1(){
        cost = 100;
        currentWeight = 10000;
        maxWeight = 18000;
    }


    public boolean launch(){
        double chance = (0.05 * (currentWeight/maxWeight));
        double randomNum = Math.random();
        System.out.println(currentWeight);
        System.out.println(maxWeight);
        System.out.println(chance);
        System.out.println(randomNum);
        if (randomNum <= chance)
            return false;
        else
            return true;
    }

    public boolean land(){
        double chance = 0.01 * (currentWeight/maxWeight);
        double randomNum = Math.random();
        if (randomNum <= chance)
            return false;
        else
            return true;
    }
}
