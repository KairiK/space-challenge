import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

public class Simulation {
    public static ArrayList loadItems(String filename) throws Exception {
        File file = new File (filename);
        Scanner scanner = new Scanner(file);
        String[] temp = new String[2];
        ArrayList<Item> itemList = new ArrayList();
        while(scanner.hasNextLine()){
            String line = scanner.nextLine();
            temp = line.split("=");
            Item item = new Item(temp[0], Integer.parseInt(temp[1]));
            itemList.add(item);
        }
        return itemList;
    }

    public static ArrayList<U1> loadU1(ArrayList<Item> itemList){
        U1 rocket = new U1();
        int size = itemList.size();
        ArrayList<U1> fullRockets = new ArrayList();
        for(int i=0; i<size; i++){
            if(rocket.canCarry(itemList.get(i))){
                rocket.carry(itemList.get(i));
            } else {
                fullRockets.add(rocket);
                rocket = new U1();
                rocket.carry(itemList.get(i));
            }
        }
        return fullRockets;
    }

    public static ArrayList<U2> loadU2(ArrayList<Item> itemList){
        U2 rocket = new U2();
        int size = itemList.size();
        ArrayList<U2> fullRockets = new ArrayList();
        for(int i=0; i<size; i++){
            if(rocket.canCarry(itemList.get(i))){
                rocket.carry(itemList.get(i));
            } else {
                fullRockets.add(rocket);
                rocket = new U2();
                rocket.carry(itemList.get(i));
            }
        }
        return fullRockets;
    }

    public static int runSimulation(ArrayList<Rocket> fullRockets) throws Exception{
        int totalBudget = 0;
        ArrayList<Rocket> allRockets = fullRockets;
        for(Rocket oneRocket : allRockets){
            if(oneRocket.launch()){
                if(oneRocket.land()){
                    totalBudget += oneRocket.cost();
                } else {
                    totalBudget += oneRocket.cost();
                    allRockets.add(oneRocket);
                }
            } else {
                totalBudget += oneRocket.cost();
                allRockets.add(oneRocket);
            }
        }


        return totalBudget;
    }
}
